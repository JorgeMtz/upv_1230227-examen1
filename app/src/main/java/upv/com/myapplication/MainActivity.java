package upv.com.myapplication;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStreamReader;


public class MainActivity extends ActionBarActivity {

    //Declarar los elementeos
   private EditText text;
    private EditText num;
   private Button calcular;
   private TextView resultado;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = (EditText) findViewById(R.id.palabra);
        num = (EditText) findViewById(R.id.numero);

        calcular = (Button) findViewById(R.id.calcular);
        resultado=(TextView) findViewById(R.id.resultado);
    }

    public void calcular(View view) {

         String charMin = "abcdefghijklmnopqrstuvwxyz";
         String charMay = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String entrada = text.getText().toString();;

        int desp= Integer.valueOf(num.getText().toString());



        String salida = "";
        for(int i = 0;i<entrada.length();i++){
            if((charMin.indexOf(entrada.charAt(i)) != -1) || (charMay.indexOf(entrada.charAt(i)) != -1))
                salida += (charMin.indexOf(entrada.charAt(i)) != -1) ? charMin.charAt( ( (charMin.indexOf(entrada.charAt(i)) )+desp)%charMin.length() ) : charMay.charAt(( charMay.indexOf(entrada.charAt(i)) +desp)%charMay.length());
            else
                salida += entrada.charAt(i);
        }

        resultado.setText("resultado: "+ salida );

    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
